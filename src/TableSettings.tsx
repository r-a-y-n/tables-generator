import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  TableSettingsColors,
  HorizontalDirection,
  VerticalDirection,
  ITableState
} from "./App";

interface ITableSettingsProps {
  color: TableSettingsColors;
  closeSettings: () => void;
  updateState: (stateVals: ITableState) => void;

  redState: ITableState;
  greenState: ITableState;
  blueState: ITableState;
}

enum DropDownOptions {
  LTRUP = "LTR-UP",
  LTRDOWN = "LTR-DOWN",
  RTLUP = "RTL-UP",
  RTLDOWN = "RTL-DOWN"
}

const StyledTableSettingsWrapper = styled.form`
  width: 20em;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin: 0.5em;
  padding: 1em;
  background-color: lightgrey;
`;

const StyledFormDataWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const StyledLabelsWrapper = styled.span`
  width: 15%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-end;
  margin: 0.5em;
`;

const StyledInputsWrapper = styled.span`
  width: 30%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  margin: 0.5em;
`;

const StyledButtonsWrapper = styled.div`
  margin: 0.5em;
  width: 90%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const TableSettings: React.FC<ITableSettingsProps> = props => {
  const [nVal, setNVal] = useState("1");
  const [xVal, setXVal] = useState("1");
  const [mVal, setMVal] = useState("25");
  const [wVal, setWVal] = useState("33");
  const [hDir, setHDir] = useState(HorizontalDirection.LTR);
  const [vDir, setVDir] = useState(VerticalDirection.Up);
  const [dropDownVal, setDropDownVal] = useState(
    DropDownOptions.LTRUP.toString()
  );

  useEffect(() => {
    switch (props.color) {
      case TableSettingsColors.Red: {
        setNVal(props.redState.nVal.toString());
        setXVal(props.redState.xVal.toString());
        setMVal(props.redState.mVal.toString());
        setWVal(props.redState.wVal.toString());
        setHDir(props.redState.hDir);
        setVDir(props.redState.vDir);
        break;
      }
      case TableSettingsColors.Green: {
        setNVal(props.greenState.nVal.toString());
        setXVal(props.greenState.xVal.toString());
        setMVal(props.greenState.mVal.toString());
        setWVal(props.greenState.wVal.toString());
        setHDir(props.greenState.hDir);
        setVDir(props.greenState.vDir);
        break;
      }
      case TableSettingsColors.Blue: {
        setNVal(props.blueState.nVal.toString());
        setXVal(props.blueState.xVal.toString());
        setMVal(props.blueState.mVal.toString());
        setWVal(props.blueState.wVal.toString());
        setHDir(props.blueState.hDir);
        setVDir(props.blueState.vDir);
        break;
      }
    }
  }, [props]);

  useEffect(() => {
    switch (dropDownVal) {
      case DropDownOptions.LTRUP: {
        setHDir(HorizontalDirection.LTR);
        setVDir(VerticalDirection.Up);
        break;
      }
      case DropDownOptions.LTRDOWN: {
        setHDir(HorizontalDirection.LTR);
        setVDir(VerticalDirection.Down);
        break;
      }
      case DropDownOptions.RTLUP: {
        setHDir(HorizontalDirection.RTL);
        setVDir(VerticalDirection.Up);
        break;
      }
      case DropDownOptions.RTLDOWN: {
        setHDir(HorizontalDirection.RTL);
        setVDir(VerticalDirection.Down);
        break;
      }
    }
  }, [dropDownVal]);

  useEffect(() => {
    if (hDir === HorizontalDirection.LTR && vDir === VerticalDirection.Up) {
      setDropDownVal(DropDownOptions.LTRUP);
      return;
    }
    if (hDir === HorizontalDirection.LTR && vDir === VerticalDirection.Down) {
      setDropDownVal(DropDownOptions.LTRDOWN);
      return;
    }
    if (hDir === HorizontalDirection.RTL && vDir === VerticalDirection.Up) {
      setDropDownVal(DropDownOptions.RTLUP);
      return;
    }
    setDropDownVal(DropDownOptions.RTLDOWN);
  }, [hDir, vDir]);

  const validateAndUpdate = (newState: ITableState) => {
    if (
      (newState.nVal + newState.xVal <= newState.mVal && newState.xVal > 0) ||
      (newState.nVal + newState.xVal >= newState.mVal && newState.xVal < 0)
    ) {
      props.updateState(newState);
      props.closeSettings();
    }
  };

  if (props.color === TableSettingsColors.None) {
    return null;
  }
  return (
    <StyledTableSettingsWrapper>
      <label>Table: {props.color}</label>
      <StyledFormDataWrapper>
        <StyledLabelsWrapper>
          <label>N = </label>
          <label>X = </label>
          <label>M = </label>
          <label>W = </label>
          <label>D = </label>
        </StyledLabelsWrapper>
        <StyledInputsWrapper>
          <input
            type="number"
            value={nVal}
            onChange={e => setNVal(e.target.value)}
          />
          <input
            type="number"
            value={xVal}
            onChange={e => setXVal(e.target.value)}
          />
          <input
            type="number"
            value={mVal}
            onChange={e => setMVal(e.target.value)}
          />
          <input
            type="number"
            value={wVal}
            onChange={e => setWVal(e.target.value)}
          />
          <select
            value={dropDownVal}
            onChange={e => setDropDownVal(e.target.value)}
            onBlur={e => setDropDownVal(e.target.value)}
          >
            <option key={DropDownOptions.LTRUP} value={DropDownOptions.LTRUP}>
              {DropDownOptions.LTRUP}
            </option>
            <option
              key={DropDownOptions.LTRDOWN}
              value={DropDownOptions.LTRDOWN}
            >
              {DropDownOptions.LTRDOWN}
            </option>
            <option key={DropDownOptions.RTLUP} value={DropDownOptions.RTLUP}>
              {DropDownOptions.RTLUP}
            </option>
            <option
              key={DropDownOptions.RTLDOWN}
              value={DropDownOptions.RTLDOWN}
            >
              {DropDownOptions.RTLDOWN}
            </option>
          </select>
        </StyledInputsWrapper>
      </StyledFormDataWrapper>
      <StyledButtonsWrapper>
        <button
          onClick={e => {
            e.preventDefault();
            validateAndUpdate({
              nVal: parseInt(nVal),
              xVal: parseInt(xVal),
              mVal: parseInt(mVal),
              wVal: parseInt(wVal),
              hDir,
              vDir
            });
          }}
        >
          OK
        </button>
        <button
          onClick={e => {
            e.preventDefault();
            props.closeSettings();
          }}
        >
          CANCEL
        </button>
      </StyledButtonsWrapper>
    </StyledTableSettingsWrapper>
  );
};

export default TableSettings;
