import React, { useState } from "react";
import Table from "./Table";
import styled from "styled-components";
import TableSettings from "./TableSettings";

export enum HorizontalDirection {
  LTR,
  RTL
}

export enum VerticalDirection {
  Up,
  Down
}

export enum TableSettingsColors {
  None,
  Red = "Red",
  Green = "Green",
  Blue = "Blue"
}

export interface ITableState {
  nVal: number;
  xVal: number;
  mVal: number;
  wVal: number;
  hDir: HorizontalDirection;
  vDir: VerticalDirection;
}

const StyledTablesWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;

  @media (max-width: 600px) {
    flex-direction: column;
  }
`;

const App: React.FC = () => {
  const [redState, setRedState]: [ITableState, Function] = useState({
    nVal: 8,
    xVal: 1,
    mVal: 29,
    wVal: 20,
    hDir: HorizontalDirection.LTR,
    vDir: VerticalDirection.Up
  });

  const [greenState, setGreenState]: [ITableState, Function] = useState({
    nVal: 231,
    xVal: 1,
    mVal: 247,
    wVal: 30,
    hDir: HorizontalDirection.LTR,
    vDir: VerticalDirection.Up
  });

  const [blueState, setBlueState]: [ITableState, Function] = useState({
    nVal: 47,
    xVal: 2,
    mVal: 81,
    wVal: 40,
    hDir: HorizontalDirection.RTL,
    vDir: VerticalDirection.Up
  });

  const [showSettingsFor, setShowSettingsFor] = useState(
    TableSettingsColors.None
  );

  const updateState = (color: TableSettingsColors, stateVals: ITableState) => {
    switch (color) {
      case TableSettingsColors.Red: {
        setRedState(stateVals);
        break;
      }
      case TableSettingsColors.Green: {
        setGreenState(stateVals);
        break;
      }
      case TableSettingsColors.Blue: {
        setBlueState(stateVals);
        break;
      }
    }
  };

  return (
    <div>
      <StyledTablesWrapper>
        <Table
          color="red"
          stateVals={redState}
          setShowSettings={() => setShowSettingsFor(TableSettingsColors.Red)}
        />
        <Table
          color="green"
          stateVals={greenState}
          setShowSettings={() => setShowSettingsFor(TableSettingsColors.Green)}
        />
        <Table
          color="blue"
          stateVals={blueState}
          setShowSettings={() => setShowSettingsFor(TableSettingsColors.Blue)}
        />
      </StyledTablesWrapper>

      <TableSettings
        color={showSettingsFor}
        closeSettings={() => setShowSettingsFor(TableSettingsColors.None)}
        updateState={(stateVals: ITableState) =>
          updateState(showSettingsFor, stateVals)
        }
        redState={redState}
        greenState={greenState}
        blueState={blueState}
      />
    </div>
  );
};

export default App;
