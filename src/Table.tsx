import React from "react";
import styled from "styled-components";
import { ITableState, HorizontalDirection, VerticalDirection } from "./App";

interface ITableProps {
  color: string;
  stateVals: ITableState;
  setShowSettings: () => void;
}

const StyledTableWrapper = styled(({ wVal, color, ...rest }) => (
  <div {...rest} />
))`
  width: ${props => props.wVal}%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border: 1px solid ${props => props.color};
  margin: 0.5em;
  min-width: 10em;

  @media (max-width: 750px) {
    display: ${props => (props.color !== "blue" ? "flex" : "none")};
  }
`;

const StyledTable = styled.table`
  border: 1px solid black;
  border-collapse: collapse;
  width: 90%;
  margin: 0.5em;
`;

const StyledTd = styled.td`
  border: 1px solid black;

  :empty {
    background-color: grey;
  }
`;

const StyledTitleWrapper = styled.div`
  margin: 0.5em;
  width: 90%;
  display: flex;
  justify-content: space-between;
  align-items: space-between;
`;

const Table: React.FC<ITableProps> = props => {
  const populateTableData = () => {
    let numList: Array<number> = [];

    if (props.stateVals.xVal >= 0) {
      for (
        let i = props.stateVals.nVal;
        i <= props.stateVals.mVal;
        i += props.stateVals.xVal
      ) {
        numList.push(i);
      }
    } else {
      for (
        let i = props.stateVals.nVal;
        i >= props.stateVals.mVal;
        i += props.stateVals.xVal
      ) {
        numList.push(i);
      }
    }

    let numEmptyCells: number = 5 - (numList.length % 5);
    let numListStr = numList.map(String);

    let segmentedList: Array<Array<string>> = [];
    let reverseRow: boolean = props.stateVals.hDir === HorizontalDirection.RTL;

    while (numListStr.length > 0) {
      let subList = numListStr.splice(0, 5);
      if (!reverseRow) {
        segmentedList.push(subList);
      }

      if (subList.length < 5) {
        for (let i = 0; i < numEmptyCells; i++) {
          subList.push("");
        }
      }

      if (reverseRow) {
        segmentedList.push(subList.reverse());
      }

      reverseRow = !reverseRow;
    }

    if (props.stateVals.vDir === VerticalDirection.Up) segmentedList.reverse();

    return segmentedList.map((subList, subListIndex) => {
      return (
        <tr key={subListIndex}>
          {subList.map((val, valIndex) => {
            return <StyledTd key={valIndex}>{val}</StyledTd>;
          })}
        </tr>
      );
    });
  };

  return (
    <StyledTableWrapper wVal={props.stateVals.wVal} color={props.color}>
      <StyledTable id="table">
        <tbody>{populateTableData()}</tbody>
      </StyledTable>
      <StyledTitleWrapper>
        <button onClick={() => props.setShowSettings()}>Configure</button>
        <label>{props.stateVals.wVal}%</label>
      </StyledTitleWrapper>
    </StyledTableWrapper>
  );
};

export default Table;
